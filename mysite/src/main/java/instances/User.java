package instances;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class User {

    private String userName;
    private String email;
    private String firstName;
    private String lastName;
    private String website;
    private String password;
    private boolean sendUserNotification;
    private String role;
    private List<String> roles;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSendUserNotification() {
        return sendUserNotification;
    }

    public void setSendUserNotification(boolean sendUserNotification) {
        this.sendUserNotification = sendUserNotification;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public void setRoles(String... roles) {
        this.roles = Arrays.asList(roles);
    }

    public String printRoles() {
        StringBuffer stringBuffer = new StringBuffer();
        for(String role : roles) {
            stringBuffer.append(role);
            stringBuffer.append(",");
        }
        return stringBuffer.toString().substring(0, stringBuffer.length() - 1);
    }

    @Override
    public String toString() {
        return String.format("Username: %s|First Name:%s|Last Name: %s|Email: %s|Roles: %s", getUserName(), getFirstName(), getLastName(), getEmail(), printRoles());
    }
}
