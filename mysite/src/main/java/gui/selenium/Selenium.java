package gui.selenium;

import gui.driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Selenium {

    private static final Logger logger = LoggerFactory.getLogger(Selenium.class);

    private static long waitInMilliseconds = 500;

    public static WebDriver driver = Driver.getdriver();
    @Deprecated
    private static WebDriverWait wait = new WebDriverWait(driver,30);

    public static WebElement findElement(String locator) {
        return findElements(locator).get(0);
    }

    public static List<WebElement> findElements(String locator) {

        List<WebElement> elementsList = new ArrayList<>();
        int attempts = 0;

        while(attempts < 10 && elementsList.size() == 0) {
            if (locator.contains("//")) {
                elementsList = driver.findElements(By.xpath(locator));
            } else {
                elementsList = driver.findElements(By.cssSelector(locator));
            }
            waitInMilliseconds(waitInMilliseconds);
            attempts++;
        }

        return elementsList;
    }

    public static void waitInMilliseconds(long milliseconds) {
        long currentTime = System.currentTimeMillis();
        long endTime = currentTime + milliseconds;
        while (currentTime < endTime) {
            currentTime = System.currentTimeMillis();
        }
    }

    public static void clickElement(String locator) {
        if(isElementClickable(locator)) {

        }
    }

    public static void writeToElement(String locator, String text) {
        WebElement element = findElement(locator);
        element.clear();
        element.sendKeys(text);
    }

    public static void clearElement(String locator) {

    }

    public static void scrollIntoView(String locator) {

    }

    public static void isElementPresent(String locator) {

    }

    public static boolean isElementClickable(String locator) {

        return true;
    }

    public static void main(String[] args) {
        List<Integer> myList = null;
        System.out.println(myList.get(0));
    }

}
