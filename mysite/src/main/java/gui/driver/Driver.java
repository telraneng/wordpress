package gui.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class Driver {

    private static final Logger logger = LoggerFactory.getLogger(Driver.class);

    public static WebDriver driver;


    public static void openChromeDriver() {
        System.setProperty("webdriver.chrome.driver", "C:/tools/webdrivers/chromedriver.exe");
        driver = new ChromeDriver();

    }

    public static WebDriver getdriver(){
        System.setProperty("webdriver.chrome.driver", "C:/tools/webdrivers/chromedriver.exe");
        if (driver == null){
            logger.info("Opening Chrome Browser");
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(15, TimeUnit.SECONDS);
//            driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
            return driver;
        }else{
            return driver;
        }
    }

    public static void closeDriver() {
        logger.info("Closing Chrome Browser");
        driver.quit();
    }

    public static void main(String[] args) {
//        Driver.getdriver();
//        Driver.closeDriver();

        getdriver();
        getdriver();
        getdriver();
        getdriver();
        getdriver();
        getdriver();
    }

}
