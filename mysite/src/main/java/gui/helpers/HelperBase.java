package gui.helpers;

import gui.driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HelperBase {

    private static final Logger logger = LoggerFactory.getLogger(HelperBase.class);
    public static WebDriver driver = Driver.getdriver();

    public static void click(By locator) {
        logger.debug("Clicking on element by locator {}", locator);
        driver.findElement(locator).click();
    }

    public static void clear(By locator) {
        logger.debug("Clearing element by locator {}", locator);
        driver.findElement(locator).clear();
    }

    public static void type(By locator, String text) {
        if (text != null) {
            click(locator);
            clear(locator);
            logger.debug("Typing text: '{}' to element by locator {}", text, locator);
            driver.findElement(locator).sendKeys(text);
        }
    }


}
