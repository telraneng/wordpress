package gui.pages.login;

public class LoginPageRepo {

    public static String poweredByWordpressLink = ".//div[@id='login']//a[contains(text(),'Powered by WordPress')]";
    public static String userNameInputField = "#user_login";
    public static String passwordInputField = "#user_pass";
    public static String loginButton = "#wp-submit";

}
