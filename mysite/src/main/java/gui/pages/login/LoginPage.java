package gui.pages.login;

import gui.pages.BasePage;
import gui.selenium.Selenium;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginPage extends BasePage {

    private static final Logger logger = LoggerFactory.getLogger(LoginPage.class);

    public static void getToPage() {
        logger.info("Navigating to Wordpress Login Page");
        driver.get("http://192.168.247.5:8080/wp-login.php");
    }

    public static boolean isOnPage() {
        logger.debug("Verifying the Page");
        return isOnPage(LoginPageRepo.poweredByWordpressLink);
    }

    public static void setUserNameInputField(String username) {
        WebElement userNameField = Selenium.findElement(LoginPageRepo.userNameInputField);
        userNameField.click();
        userNameField.clear();
        userNameField.sendKeys(username);
    }

    public static void setPasswordField(String password) {
        WebElement passwordField = Selenium.findElement(LoginPageRepo.passwordInputField);
        passwordField.click();
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public static void clickSubmitButton() {
        WebElement submit = Selenium.findElement(LoginPageRepo.loginButton);
        submit.click();
    }

}


