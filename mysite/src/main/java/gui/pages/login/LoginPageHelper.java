package gui.pages.login;

import gui.selenium.Selenium;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginPageHelper extends LoginPage{

    private static final Logger logger = LoggerFactory.getLogger(LoginPageHelper.class);

    public static void loginToWordpress(String username, String password) {
        getToPage();
        isOnPage();
        setUserNameInputField(username);
        setPasswordField(password);
        clickSubmitButton();
        Selenium.waitInMilliseconds(3000);
    }
}
