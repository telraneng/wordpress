package gui.pages.user;

import gui.pages.BasePage;
import gui.selenium.Selenium;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UsersPage extends BasePage {

    private static final Logger logger = LoggerFactory.getLogger(UsersPage.class);

    public static boolean isOnPage() {
        return isOnPage(UserPageRepo.addNewUserButton);
    }

    public static void clickAddNewUserButton() {
        Selenium.clickElement(UserPageRepo.addNewUserButton);
    }

}
