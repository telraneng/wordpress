package gui.pages.user;

public class UserPageRepo {

    // UserPage Repo
    public static String addNewUserButton = ".//a[text()='Add New']";

    // AddNewUserPage Repo

    public static String userNameInput = ".//input[@id='user_login']";
    public static String emailInput = ".//input[@id='email']";
    public static String firstNameInput = ".//input[@id='first_name']";
    public static String lastNameInput = ".//input[@id='last_name']";
    public static String webSiteInput = ".//input[@id='url']";
    public static String showPasswordButton = ".//button[text()='Show password']";
    public static String passwordInput = ".//input[@id='pass1']";
    public static String setUserNotification = ".//input[@id='send_user_notification']";
    public static String roleSelector = ".//select[@id='role']";
    public static String addNewUserSubmitButton = ".//input[@id='createusersub']";
}
