package gui.pages.user;

import gui.selenium.Selenium;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddNewUserPage {

    private static final Logger logger = LoggerFactory.getLogger(AddNewUserPage.class);

    public static void setUserName(String username) {
        Selenium.writeToElement(UserPageRepo.userNameInput, username);
    }
}
