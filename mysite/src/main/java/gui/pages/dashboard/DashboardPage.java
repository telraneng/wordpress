package gui.pages.dashboard;

import gui.helpers.HelperBase;
import gui.pages.BasePage;
import gui.pages.login.LoginPage;
import gui.selenium.Selenium;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DashboardPage extends BasePage {

    private static final Logger logger = LoggerFactory.getLogger(DashboardPage.class);

    public static boolean isOnPage() {
        return Selenium.findElements(DashboardPageRepo.dashboardMenuButton).size() > 0;
    }

    public static void clickDashboardMenuButton() {
        WebElement element = Selenium.findElement(DashboardPageRepo.dashboardMenuButton);
        element.click();
    }


}
