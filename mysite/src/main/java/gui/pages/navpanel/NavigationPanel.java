package gui.pages.navpanel;

import gui.driver.Driver;
import gui.pages.BasePage;
import gui.pages.dashboard.DashboardPage;
import gui.selenium.Selenium;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class NavigationPanel extends BasePage {

    private static final Logger logger = LoggerFactory.getLogger(NavigationPanel.class);

    public static void clickUsersMenuButton() {
        Selenium.clickElement(NavigationPanelRepo.usersMenuButton);
    }

}
