package gui.pages;

import gui.driver.Driver;
import gui.selenium.Selenium;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static gui.driver.Driver.getdriver;

public class BasePage {
    private static final Logger logger = LoggerFactory.getLogger(BasePage.class);

    public static WebDriver driver = Driver.getdriver();

    public static boolean isOnPage(String selector) {
        logger.debug("Verifying the Page");
        return isElementPresent(selector);
    }

    public static boolean isElementPresent(String locator) {
        logger.debug("Checking elements by locator {}", locator);
        return Selenium.findElements(locator).size() > 0;
    }
}
