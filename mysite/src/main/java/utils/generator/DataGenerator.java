package utils.generator;

import com.github.javafaker.Faker;
import instances.User;

public class DataGenerator {

    public static User generateRandomUser() {
        User user = new User();
        Faker faker = new Faker();
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(String.format("%s.%s@mail.com", firstName.toLowerCase(), lastName.toLowerCase()));
        user.setPassword("password");
        user.setUserName(String.format("%s.%s", firstName.toLowerCase(), lastName.toLowerCase()));
        user.setRoles("subscriber");
        user.setWebsite(faker.internet().url());
        return user;
    }
}
