package api;

import instances.User;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.*;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.generator.DataGenerator;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class ApiClient {
    private static final String BASE_URL = "http://192.168.247.5:8080/wp-json/wp/v2";
    private static final String hostName = "192.168.247.5";
    private static final int port = 8080;
    private static final String scheme = "http";
    private static final String username = "user";
    private static final String password = "password";
    private static final Logger logger = LoggerFactory.getLogger(ApiClient.class);

    public static void main(String[] args) {
        //Testing GET
        Response getResponse = sendGet(ApiEndPoints.USERS);
        System.out.println(getResponse.getStatusCode());
        System.out.println(getResponse.getBody());

        //Testing POST
        User user = DataGenerator.generateRandomUser();
        JSONObject newUserAsJson = new JSONObject();
        newUserAsJson.put("username", user.getUserName());
        newUserAsJson.put("email", user.getEmail());
        newUserAsJson.put("password", user.getPassword());
        String requestBody = newUserAsJson.toString();

        Response postResponse = sendPost(ApiEndPoints.USERS, requestBody);
        System.out.println(postResponse.getStatusCode());
        System.out.println(postResponse.getBody());

        //Testing searching user in GET response body
        boolean result = ApiUserHelper.hasUserByName(user);
        System.out.println(result);
    }

    public static Response sendGet(String endpoint)  {
        logger.debug("**************************************************");
        logger.debug("==> Preparing GET request to {}{} : ", BASE_URL, endpoint);
        HttpGet httpGet = new HttpGet(BASE_URL + endpoint);
        logger.debug("==> Authentication...");
        HttpClientContext context = setAuthCredentials();
        Response responseObject = new Response();
        logger.debug("==> Executing GET request...");
        try (CloseableHttpClient client = HttpClients.createDefault();
             CloseableHttpResponse response = client.execute(httpGet, context)
            ){
            logger.debug("==> Saving status code and body from http response to Response object...");
            responseObject.setStatusCode(response.getStatusLine().getStatusCode());
            responseObject.setBody(EntityUtils.toString(response.getEntity()));
            logger.debug("Response code from resource : {}", responseObject.getStatusCode());
            logger.debug("Response body from resource : {}", responseObject.getBody());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return responseObject;
    }

    public static Response sendPost(String endpoint, String jsonBody){
        logger.debug("**************************************************");
        logger.debug("==> Preparing POST request to {}{} : ", BASE_URL, endpoint);
        HttpPost httpPost = new HttpPost(BASE_URL + endpoint);

        logger.debug("==> Authentication...");
        HttpClientContext context = setAuthCredentials();
        logger.debug("==> Setting json body to POST request...");
        setJsonBodyToPost(httpPost, jsonBody);

        Response responseObject = new Response();
        logger.debug("==> Executing POST request...");
        try (CloseableHttpClient client = HttpClients.createDefault();
             CloseableHttpResponse response = client.execute(httpPost, context)){
            logger.debug("==> Saving status code and body from http response to Response object...");
            responseObject.setStatusCode(response.getStatusLine().getStatusCode());
            responseObject.setBody(EntityUtils.toString(response.getEntity()));
            logger.debug("Response code from resource : {}", responseObject.getStatusCode());
            logger.debug("Response body from resource : {}", responseObject.getBody());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return responseObject;
    }

    private static HttpClientContext setAuthCredentials(){
        logger.debug("==>Setting target host params...");
        HttpHost targetHost = new HttpHost(hostName, port, scheme);

        logger.debug("==> Setting basic auth credentials...");
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(username, password));

        logger.debug("==> Instantiating auth cache...");
        AuthCache authCache = new BasicAuthCache();
        authCache.put(targetHost, new BasicScheme());

        logger.debug("==> Setting context with credentials and auth cache...");
        HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(credsProvider);
        context.setAuthCache(authCache);
        return context;
    }

    private static void setJsonBodyToPost(HttpPost post, String jsonBody){
        logger.debug("==> Setting headers...");
        post.setHeader("Accept", "application/json");
        post.setHeader("Content-type", "application/json");
        logger.debug("==> Setting string entity with jsonBody ...");
        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonBody);
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        post.setEntity(entity);
    }

}
