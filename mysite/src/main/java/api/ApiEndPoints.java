package api;

public class ApiEndPoints {
    public static final String USERS = "/users";
    public static final String SEARCH = "/search";
}
