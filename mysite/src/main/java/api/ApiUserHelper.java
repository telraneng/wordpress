package api;

import instances.User;
import org.json.JSONArray;
import org.json.JSONObject;


public class ApiUserHelper {

    public static boolean hasUserByName(User user){
        Response response = ApiClient.sendGet(ApiEndPoints.USERS);
        JSONArray responseBodyAsArray = new JSONArray(response.getBody());
        for(int i = 0; i < responseBodyAsArray.length(); i++){
            JSONObject jsonObject = responseBodyAsArray.getJSONObject(i);
            if(jsonObject.get("name").equals(user.getUserName())){
                return true;
            }
        }
        return false;
    }



}
