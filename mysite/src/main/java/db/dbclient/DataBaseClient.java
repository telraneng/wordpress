package db.dbclient;

import java.sql.*;
import java.text.MessageFormat;

public class DataBaseClient {

    private static String connectionUrl = "jdbc:mysql://192.168.247.5:3306/wordpress";
    private static String dbUserName = "root";
    private static String dbPassword = "password";

    public static void connectToDataBase(String sqlQuery) {

        String sqlSelectAllPersons = sqlQuery;
        StringBuilder sb = new StringBuilder();

        try (Connection conn = DriverManager.getConnection(connectionUrl, dbUserName, dbPassword);
             PreparedStatement ps = conn.prepareStatement(sqlSelectAllPersons);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                String displayName = rs.getString("display_name");
                String userEmail = rs.getString("user_email");
                String userRegistered = rs.getString("user_registered");

                // do something with the extracted data...
                sb.append(displayName);
                sb.append("\n");
                sb.append(userEmail);
                sb.append("\n");
                sb.append(userRegistered);
                sb.append("\n");
            }

            System.out.println(sb.toString());
        } catch (SQLException e) {
            e.getStackTrace();
        }

    }

    public static Boolean hasUserByName(String userName) {
//        String sqlQuery = "SELECT * FROM wp_users WHERE display_name IN (" + userName + ");";
        String sqlQuery = String.format("SELECT * FROM wp_users WHERE display_name IN ('%s');", userName);
        connectToDataBase(sqlQuery);

        return null;
    }

    public static void main(String[] args) {
        hasUserByName("user");
    }
}
