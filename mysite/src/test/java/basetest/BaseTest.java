package basetest;

import gui.driver.Driver;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class  BaseTest {
    public static WebDriver driver;

    @BeforeClass
    public void setUp() {
        driver = Driver.getdriver();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
