package sanity.users;


import basetest.BaseTest;
import gui.pages.dashboard.DashboardPage;
import gui.pages.login.LoginPageHelper;
import gui.pages.navpanel.NavigationPanel;
import gui.pages.user.AddNewUserPage;
import gui.pages.user.UsersPage;
import instances.User;
import org.testng.annotations.Test;
import utils.generator.DataGenerator;

public class CreateNewUserTest extends BaseTest {


    @Test
    public void createNewUserWithGui() throws InterruptedException {
        User user = DataGenerator.generateRandomUser();

//        Login to Wordpress
        LoginPageHelper.loginToWordpress("user", "password");

//        From Dashboard Nav Pannel navigate to Users Page
        DashboardPage.isOnPage();
        NavigationPanel.clickUsersMenuButton();
//
////        Click Add New button
        UsersPage.isOnPage();
        UsersPage.clickAddNewUserButton();
        AddNewUserPage.setUserName(user.getUserName());
//
//        UsersPage.isOnPage();
//
//
//        UsersPage.addNewUser();

////        Verify new user added to the Users Table
//        UserPageTable.verifyUserInTableByName();

////
////        Verify new user added to DB
//        DataBaseClient.hasUserByName("user");
//        DataBaseClient.hasUser(User user);
//
////
////        Verify new user with API request
//        ApiClient.hasUserByName();
//        ApiClient.hasUser(User user);
    }




}
