package main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainTest {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        logger.info("Info from Logger");
        logger.warn("Warning from Logger");
        logger.error("Error from Logger");
        logger.debug("Debug from Logger");
    }
}
