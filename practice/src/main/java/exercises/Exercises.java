package exercises;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class Exercises {

    public static String convertNumberToBinaryNumber(int decimalNumber) {

        String binaryNumberAsString = null;
        StringBuffer sb = new StringBuffer();

        int binary[] = new int[40];
        int index = 0;

        while (decimalNumber > 0) {
            binary[index++] = decimalNumber % 2;
            decimalNumber = decimalNumber / 2;
        }
        for (int i = index - 1; i >= 0; i--) {
            sb.append(binary[i]);
        }

        binaryNumberAsString = sb.toString();

        return binaryNumberAsString;
    }

    public static String reverseString(String str) {
        if (str == null) {
            return null;
        }
        char[] chars = str.toCharArray();
        for (int i = 0, j = chars.length - 1; i < j; i++, j--) {
            char temp;
            temp = chars[i];
            chars[i] = chars[j];
            chars[j] = temp;
        }
        String str1 = new String(chars);

        return str1;
    }

    public static void count(String str) {
        if (str == null) {
            return;
        }
        char[] ch = str.toCharArray();
        int letter = 0;
        int space = 0;
        int num = 0;
        int other = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLetter(ch[i])) {
                letter++;
            } else if (Character.isDigit(ch[i])) {
                num++;
            } else if (Character.isSpaceChar(ch[i])) {
                space++;
            } else {
                other++;
            }
        }
        System.out.println("The string is : Aa kiu, I swd skieo 236587. GH kiu: sieo?? 25.33");
        System.out.println("letter: " + letter);
        System.out.println("space: " + space);
        System.out.println("number: " + num);
        System.out.println("other: " + other);
    }

    public static void sysTimeToCurrentTime() {
        long sysTimeNow = System.currentTimeMillis();
        System.out.println(sysTimeNow);

        DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS Z");
        Date result = new Date(sysTimeNow);
        System.out.println(simple.format(result));
    }

    private static int[] mergeArray(int[] arrayA, int[] arrayB) {
        int[] mergedArray = new int[arrayA.length + arrayB.length];

        int i = 0, j = 0, k = 0;

        while (i < arrayA.length && j < arrayB.length) {
            if (arrayA[i] < arrayB[j]) {
                mergedArray[k] = arrayA[i];
                i++;
                k++;
            } else {
                mergedArray[k] = arrayB[j];
                j++;
                k++;
            }
        }

        while (i < arrayA.length) {
            mergedArray[k] = arrayA[i];
            i++;
            k++;
        }

        while (j < arrayB.length) {
            mergedArray[k] = arrayB[j];
            j++;
            k++;
        }

        return mergedArray;
    }


    public static int convertStringToInteger(String str) {
//        int num = Integer.parseInt(str);
        int num = Integer.valueOf(str);

        return num;
    }


    public static void main(String[] args) {
        System.out.println(convertNumberToBinaryNumber(73));
        System.out.println();
        System.out.println(reverseString("Palamala"));
        String text = "adad dasd A dasd 12121";
        count(text);
        sysTimeToCurrentTime();

        System.out.println("---------------------------------------------------");
        System.out.println("\nDivided by 3: ");
        for (int i = 1; i < 100; i++) {
            if (i % 3 == 0)
                System.out.print(i + ", ");
        }

        System.out.println("\n\nDivided by 5: ");
        for (int i = 1; i < 100; i++) {
            if (i % 5 == 0) System.out.print(i + ", ");
        }

        System.out.println("\n\nDivided by 3 & 5: ");
        for (int i = 1; i < 100; i++) {
            if (i % 3 == 0 && i % 5 == 0) System.out.print(i + ", ");
        }
        System.out.println("\n");
        System.out.println("---------------------------------------------------");
        System.out.println(convertStringToInteger("123"));
        System.out.println("---------------------------------------------------");
// gets the value of the specified environment variable "PATH"
        System.out.println("\nEnvironment variable PATH: ");
        System.out.println(System.getenv("PATH"));


        System.out.println(System.getProperty("java.version"));
        System.out.println(System.getProperty("java.specification.version"));
        System.out.println("---------------------------------------------------");
        int[] arrayA = new int[]{-7, 12, 17, 29, 41, 56, 79};

        int[] arrayB = new int[]{-9, -3, 0, 5, 19};

        int[] mergedArray = mergeArray(arrayA, arrayB);

        System.out.println("Array A : " + Arrays.toString(arrayA));

        System.out.println("Array B : " + Arrays.toString(arrayB));

        System.out.println("Merged Array : " + Arrays.toString(mergedArray));


    }

}
