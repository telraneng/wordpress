package conversion.numbers;

public class NumberConversion {
    public static String convertDecimalToBinary(int decimalNumber) {
        String binaryNumberAsString = "";
        StringBuffer sb = new StringBuffer();

        int binary[] = new int[40];
        int index = 0;

        while(decimalNumber >0){
            binary[index++] = decimalNumber%2;
            decimalNumber = decimalNumber/2;
        }

        for(int i = index-1; i>=0; i--){
            sb.append(binary[i]);
        }

        binaryNumberAsString = sb.toString();

        return binaryNumberAsString;
    }

    public static void main(String[] args){
        convertDecimalToBinary(73);
    }
}
