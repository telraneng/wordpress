package conversion.number;

import conversion.numbers.NumberConversion;
import org.junit.Assert;
import org.junit.Test;

public class NumberConversionTest {
    @Test
    public void convertDecimalToBinaryTest(){
        Assert.assertEquals("1001001", NumberConversion.convertDecimalToBinary(73));
    }

}
