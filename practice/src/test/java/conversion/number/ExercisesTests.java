package conversion.number;

import exercises.Exercises;
import org.junit.Assert;
import org.junit.Test;

public class ExercisesTests {

    @Test
    public void convertDecimalToBinaryTest(){
        Assert.assertEquals("1001001", Exercises.convertNumberToBinaryNumber(73));

    }

    @Test
    public void reverseStringTest(){
        Assert.assertNull(Exercises.reverseString(null));
        Assert.assertEquals("alamalaP",Exercises.reverseString("Palamala"));
    }
}
